//
//  NewPicViewController.h
//  facevot
//
//  Created by 태한 김 on 12. 7. 4..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFPhotoEditorController.h"
#import "BaseViewController.h"

@interface NewPicViewController : BaseViewController <AFPhotoEditorControllerDelegate>
{
    UIImageView *imgView;
    UIToolbar   *toolBar;
}

- (void) setImage:(UIImage*) image;

@end
