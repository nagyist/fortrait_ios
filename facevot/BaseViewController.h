//
//  BaseViewController.h
//  facevot
//
//  Created by YoonBong Kim on 12. 3. 19..
//  Copyright (c) 2012년 KTH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+KNSemiModal.h"

@interface BaseViewController : UITableViewController

- (void)onMenu;
- (void)enableMenuGesture:(BOOL)enable;

@end
