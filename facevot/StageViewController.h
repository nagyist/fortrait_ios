//
//  StageViewController.h
//  facevot
//
//  Created by 태한 김 on 12. 9. 25..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseStreamController.h"
#import "LKQueue.h"

@interface StageViewController : BaseStreamController <UICollectionViewDataSource, UICollectionViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    UIButton *genderBtn, *modalBtn;
    UIView *btnBar;

    NSMutableArray *pbList;
    LKQueue *listQueue;
    UIImagePickerController *imgPicker;

    BOOL endOfList;
    UIButton *header;
    UIView *footer;
    BOOL    _reading;
}

- (void) insertPhotoUrl:(NSString*)address thumbUrl:(NSString*) thumbAddr;
- (void) hideAddButton:(BOOL) isShow;

@end
