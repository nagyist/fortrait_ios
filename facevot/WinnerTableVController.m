//
//  WinnerTableVController.m
//  facevot
//
//  Created by 김태한 on 12. 7. 8..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "AppDelegate.h"
#import "WinnerTableVController.h"
#import "NetInterface.h"
#import "DetailViewController.h"
#import "LKQueueManager.h"

#define WINQUEUE_ID     ((1==wType)?(@"WINQ_W"):((2==wType)?@"WINQ_M":@"WINQ_T"))

@implementation WinnerTableVController

-(id) initWithType:(NSUInteger) typeValue
{
    self = [super initWithStyle:UITableViewStylePlain];
    if( self ) {
        // Winner 목록은, 캐쉬된 내용의 날짜와 현재 날짜를 비교해서 자동 갱신하도록 구현하자.
        self.tableView.delegate = self;
        self.tableView.dataSource = self;

        [self.tableView registerClass:[WinnerCell class] forCellReuseIdentifier:@"winCells"];
        
        UIRefreshControl *refreshCtrl = [[UIRefreshControl alloc] init];
        [refreshCtrl setTintColor:[UIColor darkGrayColor]];
        [refreshCtrl addTarget:self action:@selector(refreshMe:) forControlEvents:UIControlEventValueChanged];
        [self setRefreshControl:refreshCtrl];

        wType = typeValue;

        winList = [[NSMutableArray alloc] initWithCapacity:20];
        listQueue = [[LKQueueManager defaultManager] queueWithName:WINQUEUE_ID];
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if( 0 == [winList count] ) {
        page = 1;
        LKQueueEntry *entry = [listQueue entryAtIndex:0];
        [winList addObjectsFromArray:(NSArray*)(entry.resources)];
        if( 20 > [winList count] )
            endOfList = YES;
        // 완전히 처음이라면, 네트워크에서 읽어와라.
        if( 0 == [winList count] )
            [self refreshMe:self.refreshControl];
    }
    // 마지막 캐쉬 한 이후 6시간 지났나? 그러면 다시 읽어온다.
    else
    {
        NSDate *savedDate;
        switch ( wType ) {
            case W_WEEK:
                savedDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"win_week_time"]; break;
            case W_MONTH:
                savedDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"win_month_time"]; break;
            case W_TOTAL:
                savedDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"win_total_time"]; break;
        }
        if( [savedDate compare:[NSDate date]] == NSOrderedAscending )
            [self refreshMe:self.refreshControl];
    }
}


// 성별이 스위치로 바뀌는 경우 사용되기 위해.
- (void) removeCurrentListAndCache
{
    [winList removeAllObjects];
    [listQueue removeAllEntries];
}

#pragma mark - UITableView dataSource, delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( 1 == section ) return 0;

    return [winList count];
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return WINNER_CELL_H;
}

- (UITableViewCell *)tableView:(UITableView *)tView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WinnerCell *cell = [tView dequeueReusableCellWithIdentifier:@"winCells" forIndexPath:indexPath];

    [cell setImageDic:winList[indexPath.row] index:indexPath.row];
    cell.delegate = self;

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // nothing to do.
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if( 1 == section ) {
        if ( endOfList )
            return nil;

        footer = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 42)];
        [footer setTitle:@"more" forState:UIControlStateNormal];
        [footer.titleLabel setFont:CS_BOLD_FONT(12.0)];
        [footer addTarget:self action:@selector(readMore) forControlEvents:UIControlEventTouchUpInside];
        [footer setBackgroundImage:[[UIImage imageNamed:@"darkBtnBack.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 13, 10, 12)] forState:UIControlStateNormal];

        return footer;
    }

    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if( 1 == section ) return 43; // more button height.

    return 0;
}

#pragma mark -
// idx : 셀 인덱스.   tIdx : 7개 중 선택된 섬네일 번호. 마지막은 큰 것.
-(void) selectedWinnerCell:(NSUInteger)idx thumbIndex:(NSUInteger)tIdx thumbView:(UIImageView*)view
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    UITableViewCell *theCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
    tRect = [theCell convertRect:view.frame toView:appDelegate.window];

    if( nil == tmpImgV ){
        tmpBackImgV = [[UIImageView alloc] initWithFrame:appDelegate.window.frame];
        [tmpBackImgV setImage:[UIImage imageNamed:@"detailBackImg.png"]];
        [tmpBackImgV setAlpha:0.0];
        [appDelegate.window addSubview:tmpBackImgV];

        tmpImgV = [[UIImageView alloc] initWithImage:view.image];
        [tmpImgV setContentMode:UIViewContentModeScaleAspectFill];
        [tmpImgV setFrame:tRect];
        [appDelegate.window addSubview:tmpImgV];
    }

    [UIView animateWithDuration:0.4 animations:^(void) {
        [tmpImgV setFrame:CGRectMake(0, (480-MY_CELL_H)/2-44, 320, MY_CELL_H*2)];
        [tmpBackImgV setAlpha:1.0];
    } completion:^(BOOL finished)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             DetailViewController *detailVC = [[DetailViewController alloc] initWithDic: winList[idx][@(tIdx)] ];
             [detailVC setTempImage:tmpImgV.image];
             [detailVC setCaller:self];

             [_superNaviCtrl pushDetailViewCtrl:detailVC];
             [tmpImgV setAlpha:0.0];
             [tmpBackImgV setAlpha:0.0];
         });
     }];
}

- (void) returnAnimation
{
    [tmpImgV setAlpha:1.0];
    [tmpBackImgV setAlpha:1.0];

    [[_superNaviCtrl navigationController] setNavigationBarHidden:NO animated:NO];

    [UIView animateWithDuration:0.4 animations:^(void) {
        [tmpImgV setFrame:tRect];
        [tmpBackImgV setAlpha:0.0];
    } completion:^(BOOL finished)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             [UIView animateWithDuration:0.2 animations:^(void) {
                 [tmpImgV setAlpha:0.0];
             } completion:^(BOOL finished) {
                 [tmpImgV removeFromSuperview];
                 [tmpBackImgV removeFromSuperview];
                 tmpImgV = nil;
                 tmpBackImgV = nil;
             }];
         });
     }];
}


#pragma mark - refresh

// 새로 갱신. 목록을 모두 지우고 맨 첫번째 페이지만 가져온다.
- (void) refreshMe:(id) refreshCtrl
{
    NSLog(@"refresh - winners");

    BOOL gender = [[NSUserDefaults standardUserDefaults] boolForKey:@"WinnerSwitch"];

    switch ( wType ) {
        case W_WEEK:
        {
            [NetInterface weeklyWinnerGender:gender page:1
                                  onComplete:^(NSDictionary *picList)
             {
                 //cell 에서 순서대로 읽을 수 있도록 winList 에 정리한다.
                 [winList removeAllObjects];
                 [self gettingList:picList];

                 [listQueue removeAllEntries];
                 [listQueue addEntryWithInfo:@{@"title":@"weekList"}
                                   resources:winList
                                     tagName:nil];

                 [[NSUserDefaults standardUserDefaults] setObject:[NSDate dateWithTimeIntervalSinceNow:21600]forKey:@"win_week_time"];

                 dispatch_async(dispatch_get_main_queue(), ^(){
                     [self.tableView reloadData];
                     [refreshCtrl endRefreshing];
                     STOP_WAIT_VIEW;
                 });
             } onError:^{
                 NOTI(@"Error on Winner list getting.");
                 dispatch_async(dispatch_get_main_queue(), ^(){
                     [refreshCtrl endRefreshing];
                     STOP_WAIT_VIEW;
                 });
             }];
        }
            break;

        case W_MONTH:
        {
            [NetInterface monthlyWinnerGender:gender page:1
                                   onComplete:^(NSDictionary *picList)
             {
                 [winList removeAllObjects];
                 [self gettingList:picList];

                 [listQueue removeAllEntries];
                 [listQueue addEntryWithInfo:@{@"title":@"monthList"}
                                   resources:winList
                                     tagName:nil];

                 [[NSUserDefaults standardUserDefaults] setObject:[NSDate dateWithTimeIntervalSinceNow:21600]forKey:@"win_month_time"];

                 dispatch_async(dispatch_get_main_queue(), ^(){
                     [self.tableView reloadData];
                     [refreshCtrl endRefreshing];
                     STOP_WAIT_VIEW;
                 });
             } onError:^{
                 NOTI(@"Error on Winner list getting.");
                 dispatch_async(dispatch_get_main_queue(), ^(){
                     [refreshCtrl endRefreshing];
                     STOP_WAIT_VIEW;
                 });
             }];
        }
            break;

        case W_TOTAL:
        {
            [NetInterface totalWinnerGender:gender page:1
                                  onComplete:^(NSDictionary *picList)
             {
                 [winList removeAllObjects];
                 [self gettingList:picList];

                 [listQueue removeAllEntries];
                 [listQueue addEntryWithInfo:@{@"title":@"totalList"}
                                   resources:winList
                                     tagName:nil];

                 [[NSUserDefaults standardUserDefaults] setObject:[NSDate dateWithTimeIntervalSinceNow:21600]forKey:@"win_total_time"];

                 dispatch_async(dispatch_get_main_queue(), ^(){
                     [self.tableView reloadData];
                     [refreshCtrl endRefreshing];
                     STOP_WAIT_VIEW;
                 });
             } onError:^{
                 NOTI(@"Error on Winner list getting.");
                 dispatch_async(dispatch_get_main_queue(), ^(){
                     [refreshCtrl endRefreshing];
                     STOP_WAIT_VIEW;
                 });
             }];
        }
            break;
    }
}

- (void) readMore
{
    NSLog(@"read more.");
    page++;
    BOOL gender = [[NSUserDefaults standardUserDefaults] boolForKey:@"WinnerSwitch"];

    switch ( wType ) {
        case W_WEEK:
        {
            [NetInterface weeklyWinnerGender:gender page:page
                                  onComplete:^(NSDictionary *picList)
             {
                 [self gettingList:picList];
                 if( 20 > [picList count] )
                     endOfList = YES;

                 dispatch_async(dispatch_get_main_queue(), ^(){
                     [self.tableView reloadData];
                 });
             }
                                 onError:^
             {
                 NOTI(@"Network Error");
             }];
        }
            break;

        case W_MONTH:
        {
            [NetInterface monthlyWinnerGender:gender page:page
                                   onComplete:^(NSDictionary *picList)
             {
                 [winList removeAllObjects];
                 [self gettingList:picList];

                 dispatch_async(dispatch_get_main_queue(), ^(){
                     [self.tableView reloadData];
                 });
             } onError:^{
                 NOTI(@"Network Error");
             }];
        }
            break;

        case W_TOTAL:
        {
            [NetInterface totalWinnerGender:gender page:page
                                  onComplete:^(NSDictionary *picList)
             {
                 [self gettingList:picList];
                 if( 20 > [picList count] )
                     endOfList = YES;

                 dispatch_async(dispatch_get_main_queue(), ^(){
                     [self.tableView reloadData];
                 });
             }
                                     onError:^
             {
                 NOTI(@"Network Error");
             }];
        }
            break;
    }
}

// 서버에서 전달받은 데이터를, 각 셀 별로 같은 순위가 있는 형태로 변환해야 한다.
// 하나의 셀을 위해, NSDictionary 가 구성되며, 각각 NSNumber 키 값으로 0 ~ 6 에 데이터 Dictionary 가 있게 된다.
- (void) gettingList:(NSDictionary*) picList
{
    NSDictionary *ta0, *ta1, *ta2, *ta3, *ta4, *ta5, *ta6;
    for( NSUInteger ix = 0; ix < 20; ix++ ) {
        if( nil != picList[@"0"] && [(NSArray*)(picList[@"0"]) count] > ix )
            ta0 = ((NSArray*)picList[@"0"])[ix];
        else ta0 = @{@"id":@"0"};
        if( nil != picList[@"1"] && [(NSArray*)(picList[@"1"]) count] > ix )
            ta1 = ((NSArray*)picList[@"1"])[ix];
        else ta1 = @{@"id":@"0"};
        if( nil != picList[@"2"] && [(NSArray*)(picList[@"2"]) count] > ix )
            ta2 = ((NSArray*)picList[@"2"])[ix];
        else ta2 = @{@"id":@"0"};
        if( nil != picList[@"3"] && [(NSArray*)(picList[@"3"]) count] > ix )
            ta3 = ((NSArray*)picList[@"3"])[ix];
        else ta3 = @{@"id":@"0"};
        if( nil != picList[@"4"] && [(NSArray*)(picList[@"4"]) count] > ix )
            ta4 = ((NSArray*)picList[@"4"])[ix];
        else ta4 = @{@"id":@"0"};
        if( nil != picList[@"5"] && [(NSArray*)(picList[@"5"]) count] > ix )
            ta5 = ((NSArray*)picList[@"5"])[ix];
        else ta5 = @{@"id":@"0"};
        if( nil != picList[@"total"] && [(NSArray*)(picList[@"total"]) count] > ix )
            ta6 = ((NSArray*)picList[@"total"])[ix];
        else ta6 = @{@"id":@"0"};

        if( [ta0[@"id"] isEqualToString:@"0"] && [ta1[@"id"] isEqualToString:@"0"] &&
           [ta2[@"id"] isEqualToString:@"0"] && [ta3[@"id"] isEqualToString:@"0"] &&
           [ta4[@"id"] isEqualToString:@"0"] && [ta5[@"id"] isEqualToString:@"0"] &&
           [ta6[@"id"] isEqualToString:@"0"] )
            break;

        NSDictionary *tCellDic = @{@(0):ta0, @(1):ta1, @(2):ta2, @(3):ta3,
        @(4):ta4, @(5):ta5, @(6):ta6 };
        [winList addObject:tCellDic];
    }
    NSLog(@"win list ;\n%@", winList);
}

@end
