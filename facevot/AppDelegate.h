//
//  AppDelegate.h
//  SwipeMenuDemo
//
//  Created by YoonBong Kim on 12. 3. 19..
//  Copyright (c) 2012년 KTH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StageViewController.h"
#import "MenuViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    StageViewController *stageViewCntrlr;
    MenuViewController *menuViewCntrlr;
}

@property (strong, nonatomic) UIWindow *window;

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI
                            handler:( void(^)(BOOL)) handler;
extern NSString *const FBSessionStateChangedNotification;

@end
