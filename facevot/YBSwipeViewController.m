//
//  YBSwipeViewController.m
//  LeftMenuDemo
//
//  Created by YoonBong Kim on 12. 3. 5..
//  Copyright (c) 2012년 KTH. All rights reserved.
//

#import "YBSwipeViewController.h"
#import "AppDelegate.h"

#define kRevealWidth                175.0f


@implementation YBSwipeViewController


- (id)initWithMainViewController:(UIViewController *)mainViewCntrlr menuViewController:(UIViewController *)menuViewCntrlr
{
    self = [super init];
    if (self) {
        _mainViewCntrlr  = mainViewCntrlr;
        _menuViewCntrlr = menuViewCntrlr;
        
        _status = ViewStatusMain;
        
        _normPointX = 0.0f;
        _beginPointX = 0.0f;

        self.restorationIdentifier = @"rkSwipeViewController";
    }
    return self;
}




- (void)loadView
{
    [super loadView];
    
    self.wantsFullScreenLayout = YES;
    [self.view setBackgroundColor:[UIColor darkGrayColor]];

    
    /* view for main */        
    _mainView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _mainView.backgroundColor = [UIColor clearColor];

    [self.view addSubview:_mainView];

    {
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_mainView.bounds];
        [_mainView.layer setMasksToBounds:NO];
        [_mainView.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [_mainView.layer setShadowOffset:CGSizeMake(-3.0f, 0.0f)];
        [_mainView.layer setShadowOpacity:1.0f];
        [_mainView.layer setShadowRadius:14.0f];
        [_mainView.layer setShadowPath:shadowPath.CGPath];
        
        
        _menuGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        _menuGestureRecognizer.maximumNumberOfTouches = 1;
        _menuGestureRecognizer.enabled = YES;
        
        [_mainView addGestureRecognizer:_menuGestureRecognizer];
    }

    [_mainView addSubview:_mainViewCntrlr.view];    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    _mainView = nil;
    _menuView = nil;
}


- (void)setMenuGestureEnable:(BOOL)menuGestureEnable
{
    _menuGestureEnable = menuGestureEnable;
    _menuGestureRecognizer.enabled = _menuGestureEnable;
}


- (NSUInteger) supportedInterfaceOrientations
{
    return( UIInterfaceOrientationPortrait|
           UIInterfaceOrientationPortraitUpsideDown);
}


- (void)setSearchMode:(BOOL)search animated:(BOOL)animated   {
    
    [UIView animateWithDuration:0.3f animations:^{
        
        if (search) {
            _mainView.transform = CGAffineTransformMakeTranslation(320.0f, 0.0f);
        }
        else {
            _mainView.transform = CGAffineTransformMakeTranslation(kRevealWidth, 0.0f);
        }
    }];
}


- (void)setShowMenu:(BOOL)show animated:(BOOL)animated
{
    if (animated)
    {
        if (show)
        {
            if (_menuView == nil) {
                
                _menuView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

                [self.view insertSubview:_menuView belowSubview:_mainView];
                
                [_menuView addSubview:_menuViewCntrlr.view];
            }
            
//            _menuView.transform = CGAffineTransformMakeScale(0.7, 0.7);
            [_menuViewCntrlr viewWillAppear:YES];
        }
        else {
            
            [_mainViewCntrlr viewWillAppear:YES];
            [_menuViewCntrlr viewWillDisappear:YES];
        }
        
        
        [UIView animateWithDuration:0.2f animations:^{
            
            _mainView.transform = (show)? CGAffineTransformMakeTranslation(kRevealWidth, 0.0f) : CGAffineTransformMakeTranslation(0.0f, 0.0f);
//            _menuView.transform = (show)? CGAffineTransformMakeScale(1.0, 1.0) : CGAffineTransformMakeScale(0.7, 0.7);

        } completion:^(BOOL finished) {
            
                //_menuGestureRecognizer.enabled = (show)? YES : NO;
            _status = (show)? ViewStatusMenu : ViewStatusMain;
            
            if (show)   {
                
                [_menuViewCntrlr viewDidAppear:YES];
                
                BOOL interaction = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)? YES : NO;

                [[[(UINavigationController *)_mainViewCntrlr visibleViewController] view] setUserInteractionEnabled:interaction];
                
            }
            else {
        
                [_mainViewCntrlr viewDidAppear:YES];
                [_menuViewCntrlr viewDidDisappear:YES];
                
                if (_menuView != nil) {
                    [_menuView removeFromSuperview];
                    _menuView = nil;
                }
                                
                [[[(UINavigationController *)_mainViewCntrlr visibleViewController] view] setUserInteractionEnabled:YES];
            }
        }];
        
    }
    else {
        _mainView.transform = (show)?  CGAffineTransformMakeTranslation(kRevealWidth, 0.0f) : CGAffineTransformMakeTranslation(0.0f, 0.0f);
//        _menuView.transform = (show)? CGAffineTransformMakeScale(1.0, 1.0) : CGAffineTransformMakeScale(0.7, 0.7);
    }

    // Stage View Controller 에서 네비게이션 바 하단 중앙에 있는, 경계선에 걸친 버튼을
    // 사라지거나 다시 나타나게 민들기 위해 처리함.
    UINavigationController *nav = (UINavigationController *)_mainViewCntrlr;
    if( [nav.topViewController respondsToSelector:@selector(hideAddButton:)] )
        [(StageViewController*)(nav.topViewController) hideAddButton:show];
}


- (void)setSelectMenu:(UIViewController *)viewCntrlr animated:(BOOL)animated
{    
    if (animated) {
        
        [UIView animateWithDuration:0.3f animations:^{
            
            _mainView.transform = CGAffineTransformMakeTranslation(320.0f, 0.0f);

        } completion:^(BOOL finished) {

            if ([_mainViewCntrlr isKindOfClass:[UINavigationController class]]) {                
                [(UINavigationController *)_mainViewCntrlr setViewControllers:@[viewCntrlr]];
            }
            
            [self setShowMenu:NO animated:YES];
        }];
    }
    else {
        [self setShowMenu:NO animated:YES];
    }
}


#pragma mark - UIPanGestureRecognizer selector

- (void)onSwipe:(UIPanGestureRecognizer *)recognizer    {
    
    UIGestureRecognizerState state = recognizer.state;
    CGFloat pointX = [recognizer locationInView:self.view].x;
    
    switch (state) {
        case UIGestureRecognizerStateBegan:
        {
            _beginPointX = pointX;
            _normPointX = (self.status == ViewStatusMain)? 0.0f : kRevealWidth;
            _normPointB = (self.status == ViewStatusMain)? 200.0f : kRevealWidth;
            // Stage View Controller 에서 네비게이션 바 하단 중앙에 있는, 경계선에 걸친 버튼을
            // 사라지거나 다시 나타나게 민들기 위해 처리함.
            UINavigationController *nav = (UINavigationController *)_mainViewCntrlr;
            if( [nav.topViewController respondsToSelector:@selector(hideAddButton:)] )
                [(StageViewController*)(nav.topViewController) hideAddButton:YES];
        }
            break;

        case UIGestureRecognizerStateEnded:
        {
            CGFloat move = pointX - _beginPointX;
            
            if (_status == ViewStatusMain) {
                
                if (_beginPointX > pointX) {   
                    return;
                }
                
                _status = (pointX > kRevealWidth / 2)? ViewStatusMenu : ViewStatusMain;
            }
            else if(_status == ViewStatusMenu)   {
                
                
                if (-kRevealWidth > move) {
                    return;
                }
                
                _status = (pointX < kRevealWidth)? ViewStatusMain : ViewStatusMenu;
            }
            else {
                
            }
            
            switch (_status) {
                case ViewStatusMenu:
                    [self setShowMenu:YES animated:YES];
                    break;
                case ViewStatusMain:
                    [self setShowMenu:NO animated:YES];
                    break;
                default:
                    break;
            }
        }           
            break;
        case UIGestureRecognizerStateChanged:
            
            if (_menuView == nil) {
                
                _menuView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                
                [self.view insertSubview:_menuView belowSubview:_mainView];
                
                [_menuView addSubview:_menuViewCntrlr.view];
            }
            
            CGFloat move = pointX - _beginPointX;
            NSLog(@"move : %f", move);
            
            if (self.status == ViewStatusMain) {
                
                if (_beginPointX > pointX)
                    return;
            }
            else {
                if (-kRevealWidth > move)
                    return;
            }

            _mainView.transform = CGAffineTransformMakeTranslation(_normPointX + move, 0.0f);

//            CGFloat d = ((_normPointB + (move/5.0)) / kRevealWidth);
//            _menuView.transform = CGAffineTransformMakeScale(d, d);
            break;

        default:
            break;
    }
}


#pragma mark - Restorable State

-(void) encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];

    [coder encodeInteger:_status forKey:@"kStatus"];
    [coder encodeDouble:_normPointX forKey:@"kNormPointX"];
    [coder encodeDouble:_beginPointX forKey:@"kBeginPointX"];
}

-(void) decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];

    _status = [coder decodeIntegerForKey:@"kStatus"];

    _normPointX = [coder decodeDoubleForKey:@"kNormPointX"];
    _beginPointX = [coder decodeDoubleForKey:@"kBeginPointX"];
}

@end
