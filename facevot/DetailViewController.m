//
//  DetailViewController.m
//  LeftMenuDemo
//
//  Created by Blade Kim on 12. 3. 5..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DetailViewController.h"
#import "MainViewController.h"
#import "NetInterface.h"
#import "PCPieChart.h"
#import "TSActionSheet.h"
#import "TSPopoverController.h"
#import "AppDelegate.h"

@implementation DetailViewController

- (id)initWithDic:(NSDictionary*) dicInfo
{
    self = [super init];
 
    if (self) {
        // 화면을 탭 하면 그냥 다시 닫히도록 하자.
        UITapGestureRecognizer *tg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeAction:)];
        [self.view addGestureRecognizer:tg];

        _imgView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [_imgView setContentMode:UIViewContentModeScaleAspectFit];
        [_imgView setTag:1973];
        [self.view addSubview:_imgView];

        // 테두리 없는 버튼 아이템을 만든다.
        UIButton *bBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 22)];
        [bBtn setBackgroundImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
        [bBtn addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:bBtn];

        UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

        UIButton *sBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 22)];
        [sBtn setBackgroundImage:[UIImage imageNamed:@"shareBtn.png"] forState:UIControlStateNormal];
        [sBtn addTarget:self action:@selector(shareAction) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *shareBtn = [[UIBarButtonItem alloc] initWithCustomView:sBtn];

        head = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 20, 320, 44)];
        [head setBackgroundImage:[UIImage imageNamed:@"navigationBar.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        [head setAlpha:0.7f];
        [head setItems:@[backItem,flexible,shareBtn]];

        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 200, 43)];
        [nameLabel setBackgroundColor:[UIColor clearColor]];
        [nameLabel setTextColor:[UIColor blackColor]];
        [nameLabel setFont:CS_BOLD_FONT(14.0)];
        [nameLabel setTextAlignment:UITextAlignmentCenter];

        // identity 값이 같으면, 나 자신의 사진을 보는 경우로 간주하고 내 이름을 보여준다.
        if( [dicInfo[@"identity"] isEqualToString:USERCONTEXT.myIdentity] )
            [nameLabel setText:USERCONTEXT.myName];
        else {
            // 낮선 사람이라면 페이스북에 identity 를 조회해서 정보를 얻는다.
            /* {"id": "777739408",
             "name": "\uae40\ud0dc\ud55c",
             "first_name": "\ud0dc\ud55c",
             "last_name": "\uae40",
             "link": "https://www.facebook.com/blade.kim",
             "username": "blade.kim",
             "gender": "male",
             "locale": "ko_KR"  } */
//            [FBRequest startWithGraphPath:dicInfo[@"identity"]
//             completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                 [nameLabel setText: [result objectForKey:@"name"]];
//                 facebookLink = [result objectForKey:@"link"];
//             }];
            FBRequest *req = [FBRequest requestForGraphPath:dicInfo[@"identity"]];
            [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                [nameLabel setText: [result objectForKey:@"name"]];
                facebookLink = [result objectForKey:@"link"];
            }];
        }
        [head addSubview:nameLabel];

        info = dicInfo;

        // 투표하거나 결과를 알 수 있는 버튼
        UIBarButtonItem *voteItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"t_vote.png"] style:UIBarButtonItemStylePlain target:self action:@selector(btnAction:)];

        UIBarButtonItem *fbItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"t_fbook.png"] style:UIBarButtonItemStylePlain target:self action:@selector(showFacebookPage)];
        if( [dicInfo[@"identity"] isEqualToString:USERCONTEXT.myIdentity] )
            fbItem.enabled = NO;  // 나 자신은 필요 없지 않나?

        UIBarButtonItem *favoItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"t_favo.png"] style:UIBarButtonItemStylePlain target:self action:@selector(favorateBtnAction:)];

        if( [info[@"vote"] isEqualToNumber:@(1)] ) // 투표 했으면 버튼은 챠트 보기용.
            [(UIBarButtonItem*)(tail.items[0]) setImage:[UIImage imageNamed:@"t_chart.png"]];
        if( info[@"favorite"] ) // 즐겨찾기 했으면 버튼은 해제용.
            [(UIBarButtonItem*)(tail.items[2]) setImage:[UIImage imageNamed:@"t_unfavo.png"]];

        tail = [[UIToolbar alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-51, 320, 51)];
        [tail setBackgroundImage:[UIImage imageNamed:@"toolBar.png"] forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
        [tail setAlpha:0.7];
        [tail setItems:@[voteItem,fbItem,favoItem]];

        [((AppDelegate*)[UIApplication sharedApplication].delegate).window addSubview:head];
        [((AppDelegate*)[UIApplication sharedApplication].delegate).window addSubview:tail];

        checkMark = nil;
    }
    return self;
}

- (void) setTempImage:(UIImage*) img
{
    placeholderImage = img;

    // 비율로 세로로 좀 긴 사진은, 채우기 형태를 다르게 한다.
    if( img.size.height*2 > 460.0 && img.size.width*2 < 320.0 )
        [_imgView setContentMode:UIViewContentModeScaleAspectFill];
    else
        [_imgView setContentMode:UIViewContentModeScaleAspectFit];
}

- (void) setCaller:(id)vc
{
    mvc = vc;
}

- (void)loadView
{
    [super loadView];
    [self.view setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];

    [self.tableView setSeparatorColor:[UIColor clearColor]];
    [self.tableView setScrollEnabled:NO];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [_imgView setImageWithURL:[NSURL URLWithString:info[@"url"]]
     placeholderImage:placeholderImage];

    // Facebook 로그인이 되어 있는지 확인하자.
    if( ![FBSession.activeSession isOpen] )
    {
//        if( ![NetInterface facebookLogin:^(BOOL result)
//         { } showUI:NO] )
//        {
            [NetInterface facebookLogin:^(BOOL result)
             { } showUI:YES];
//        }
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -

- (void) shareAction
{
    UIActivityViewController *avc = [[UIActivityViewController alloc] initWithActivityItems:@[_imgView.image,@"address"]
                                                                      applicationActivities:nil];
    [self presentViewController:avc animated:YES completion:^(void){

    }];
}

-(void) closeAction:(id)sender
{
    [head removeFromSuperview];
    [tail removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];

    [(MainViewController*)mvc returnAnimation];
}

-(void) showFacebookPage
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:facebookLink]];
}

#pragma mark - Vote Query

// 내가 투표했는지 알아보고, 투표했으면 현황을 보여주고, 그렇지 않다면 투표 할 수 있게 해준다.
// 버튼을 누르면 여기로.
-(void) btnAction:(UIButton*)sender
{
    if( [info[@"id"] isEqualToString:@"0"] ){  // 아직 id가 없다.
        [self displayCanNotVoteNow];
        return;
    }

    if( [info[@"vote"] isEqualToNumber:@(1)] )
        [self displayChartOnPannel];   // 했다.
    else
        [self displayVoteBoard];       // 안했다.
}

// 사진의 사람에 대한 즐겨찾기 토글 버튼.
-(void) favorateBtnAction:(UIButton*) sender
{
    BOOL remember;
    if( info[@"favorite"] ){      // 해지.
        remember = NO;
    } else {                // 기억.
        remember = YES;
    }

    [sender setEnabled:NO];

    [NetInterface setFavoriteManId:info[@"identity"]
                      withPhottoId:info[@"id"]
                         boolValue:remember
                              name:[nameLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                        onComplete:^(BOOL result)
     {
         if( !remember )
             [(UIBarButtonItem*)(tail.items[2]) setImage:[UIImage imageNamed:@"t_favo.png"]];
         else
             [(UIBarButtonItem*)(tail.items[2]) setImage:[UIImage imageNamed:@"t_unfavo.png"]];

         // 이 값이 참이면 즐겨찾기 목록 뷰 화면으로 갈 때 자동으로 리로딩한다.
         USERCONTEXT.needReloadFavo = YES;

         // 새로 바뀐 정보를 가지고 있는 데이터에 반영한다.
         NSMutableDictionary *newDic = [[NSMutableDictionary alloc] initWithDictionary:info];
         NSNumber *newVal = [NSNumber numberWithBool:remember];
         [newDic setObject:newVal forKey:@"favorite"];

         info = newDic;

         [USERCONTEXT completeView];
         [sender setEnabled:YES];
     }
                           onError:^()
     {
         NOTI(@"Network Error");
         [sender setEnabled:YES];
     }];
}

// 선택한 표를 던진다.
-(void) doVote:(NSUInteger) choice
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    [tail.items[0] setEnabled:NO];

    [NetInterface doVoteAtPhotoID:info[@"id"] vote:choice
                       onComplete:^
    {
        // 새로 바뀐 정보를 가지고 있는 데이터에 반영한다.
        NSMutableDictionary *newDic = [[NSMutableDictionary alloc] initWithDictionary:info];
        [newDic setValue:@(1) forKey:@"vote"];
        NSUInteger nm = [(NSNumber*)(newDic[@"votes"][choice]) integerValue] + 1;
        NSMutableArray *newVotes = [[NSMutableArray alloc] initWithArray:newDic[@"votes"]];
        [newVotes replaceObjectAtIndex:choice withObject:@(nm)];
        [newDic setObject:newVotes forKey:@"votes"];
        
        info = newDic;

        dispatch_async(dispatch_get_main_queue(), ^(){
            NSLog(@"vote OK.");
            NSString *msg = [NSString stringWithFormat:@"%@ is %@!",([info[@"sex"] integerValue]?@"She":@"He"), ITEM_TXT([info[@"sex"] integerValue])[choice] ];
            [[[UIAlertView alloc] initWithTitle:@"OK" message:msg delegate:nil cancelButtonTitle:@"Confirm" otherButtonTitles:nil] show];

            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            // 투표 했으면 버튼은 챠트 보기용.
            [(UIBarButtonItem*)(tail.items[0]) setImage:[UIImage imageNamed:@"t_chart.png"]];

            [tail.items[0] setEnabled:YES];
        });
    } onError:^{
        dispatch_async(dispatch_get_main_queue(), ^(){
            NOTI(@"Can not vote now.");
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        [tail.items[0] setEnabled:YES];
    }];
}

-(void) displayCanNotVoteNow
{
    UILabel *msg = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 180, 60)];
    [msg setTextAlignment:NSTextAlignmentCenter];
    [msg setBackgroundColor:[UIColor clearColor]];
    [msg setTextColor:[UIColor lightGrayColor]];
    [msg setShadowColor:[UIColor blackColor]];
    [msg setShadowOffset:CGSizeMake(0, 1)];
    [msg setFont:CS_FONT(11)];
    [msg setText:@"Oops,\nplease try next time."];

    UIViewController *msgCtrl = [[UIViewController alloc] init];
    [msgCtrl setView:msg];
    TSPopoverController *popCtrl = [[TSPopoverController alloc] initWithContentViewController:msgCtrl];
    [popCtrl showPopoverWithRect:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-40, 50, 20)];

    [waitIndicator stopAnimating];
    [waitIndicator removeFromSuperview];
};

-(void) displayChartOnPannel
{
    NSArray *votes = info[@"votes"];
    NSArray *txtary = ITEM_TXT([info[@"sex"] integerValue]);
    NSArray *vArray = @[
    [PCPieComponent pieComponentWithTitle:txtary[0] value:((NSNumber*)votes[0]).integerValue color:COLOR_0],
    [PCPieComponent pieComponentWithTitle:txtary[1] value:((NSNumber*)votes[1]).integerValue color:COLOR_1],
    [PCPieComponent pieComponentWithTitle:txtary[2] value:((NSNumber*)votes[2]).integerValue color:COLOR_2],
    [PCPieComponent pieComponentWithTitle:txtary[3] value:((NSNumber*)votes[3]).integerValue color:COLOR_3],
    [PCPieComponent pieComponentWithTitle:txtary[4] value:((NSNumber*)votes[4]).integerValue color:COLOR_4],
    [PCPieComponent pieComponentWithTitle:txtary[5] value:((NSNumber*)votes[5]).integerValue color:COLOR_5]
    ];

    PCPieChart *chartView = [[PCPieChart alloc] initWithFrame:CGRectMake(0, 0, 240, 180)];
    [chartView setDiameter:110];
    [chartView setTitleFont:CS_BOLD_FONT(10.0)];
    [chartView setPercentageFont:CS_BOLD_FONT(12.0)];
    [chartView setSameColorLabel:YES];
    [chartView setShowArrow:NO];
    [chartView setComponents:[vArray mutableCopy]];
    
    TSPopoverController *popCtrl = [[TSPopoverController alloc] initWithView:chartView];

    if( nil == popCtrl ) return; // 중복 팝업인 경우 nil 이 반환된다.

    [popCtrl setPopoverBaseColor:CS_RGBA(10, 10, 10, 0.5)];
    [popCtrl showPopoverWithRect:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-40, 50, 20)];
}

-(void) displayVoteBoard
{
    TSActionSheet *voteActSheet = [[TSActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"%@ is...",([info[@"sex"] integerValue]?@"She":@"He")]];

    if( nil == voteActSheet ) return; // 중복 팝업인 경우 nil 이 반환된다.

    [voteActSheet setBackgroundColor:CS_RGB(20, 20, 20)];

    for(NSUInteger i = 0; i <= 5; i++ ){
        [voteActSheet addButtonWithTitle:ITEM_TXT([info[@"sex"] integerValue])[i]
                                   color:[UIColor clearColor]
                              titleColor:ITEM_COR[i]
                             borderWidth:1.0 borderColor:[UIColor blackColor] block:^{
                                 [self doVote:i];
                             }];
    }
    [voteActSheet showWithRect:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-40, 50, 20)];
}

#pragma mark - Vote Pannel

//- (void) setShowPannel:(BOOL)show
//{
//    [UIView animateWithDuration:0.2f animations:^{
//        votePannelView.transform = (show)?  CGAffineTransformMakeTranslation(0.0f, -360.0f) : CGAffineTransformMakeTranslation(0.0f, 0.0f);
//    } completion:^(BOOL finished) {}];
//}

//- (void)onSwipe:(UIPanGestureRecognizer *)recognizer
//{
//    UIGestureRecognizerState state = recognizer.state;
//    CGFloat pointY = [recognizer locationInView:self.view].y;
//
//    switch (state) {
//        case UIGestureRecognizerStateBegan:
//            _beginPointY = pointY;
//            _normPointY = (status == pannelStatusMain)? 0.0f : -360.0;
//            break;
//
//        case UIGestureRecognizerStateEnded:
//        {
//            CGFloat move = _beginPointY - pointY;
//            NSLog(@"end pointY %f",pointY);
//            if (status == pannelStatusMain)
//            {
//                if (_beginPointY < pointY) return;
//
//                status = (move > (360 / 4))? pannelStatusMenu : pannelStatusMain;
//            }
//            else if(status == pannelStatusMenu)
//            {
//                if (360 < move) return;
//
//                status = ((-1*move) > (360 / 4))? pannelStatusMain : pannelStatusMenu;
//            }
//
//            switch (status) {
//                case pannelStatusMenu:
//                    [self setShowPannel:YES]; break;
//                case pannelStatusMain:
//                    [self setShowPannel:NO]; break;
//                default:
//                    break;
//            }
//        }
//            break;
//
//        case UIGestureRecognizerStateChanged:
//        {
//            CGFloat move = _beginPointY - pointY;
//            NSLog(@"move : %f", _normPointY - move);
//
//            if (-360 > _normPointY - move || 0 < _normPointY - move )
//                return;
//
////            votePannelView.transform = CGAffineTransformMakeTranslation(0, _normPointY - move);
//        }
//            break;
//
//        default:
//            break;
//    }
//}

//- (void) voteCheckAction:(NSUInteger) idx
//{
//    if( nil == checkMark ){
//        checkMark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkMark.png"]];
//        [checkMark setFrame:CGRectMake(0, 0, 40, 40)];
////        [votePannelView addSubview:checkMark];
//    }
//    [checkMark setFrame:CGRectOffset(sender.frame, 4, -10)];
//    [checkMark setTag:idx]; // remember the choice.
//}

@end
