//
//  WinnerViewController.m
//  facevot
//
//  Created by 태한 김 on 12. 7. 2..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "WinnerViewController.h"
#import "WinnerTableVController.h"

@implementation WinnerViewController

-(id) init
{
    self = [super init];
    if( self ) {
        //
        WinnerTableVController *w = [[WinnerTableVController alloc] initWithType:W_WEEK];
        WinnerTableVController *m = [[WinnerTableVController alloc] initWithType:W_MONTH];
        WinnerTableVController *t = [[WinnerTableVController alloc] initWithType:W_TOTAL];
        [w setTitle:@"week"];
        [m setTitle:@"month"];
        [t setTitle:@"total"];
        [w setSuperNaviCtrl:self];
        [m setSuperNaviCtrl:self];
        [t setSuperNaviCtrl:self];
        vArray = @[ w, m, t ];

        [self.tableView setScrollEnabled:NO];
        [self.tableView setSeparatorColor:[UIColor clearColor]];
        [self.tableView setBackgroundColor:THEME_COLOR];

        // 우측 상단에는 남/여를 선택할 수 있는 스위치가 있다.
        genderBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 27)];
        if( [[NSUserDefaults standardUserDefaults] boolForKey:@"WinnerSwitch"] )
            [genderBtn setBackgroundImage:[UIImage imageNamed:@"g_woman.png"] forState:UIControlStateNormal];
        else
            [genderBtn setBackgroundImage:[UIImage imageNamed:@"g_man.png"] forState:UIControlStateNormal];
        
        [genderBtn addTarget:self action:@selector(toggleGender:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *bItem = [[UIBarButtonItem alloc] initWithCustomView:genderBtn];
        self.navigationItem.rightBarButtonItem = bItem;
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];

    segmentPageController = [[JCMSegmentPageController alloc] init];
    
    segmentPageController.delegate = self;
    segmentPageController.viewControllers = vArray;

    [segmentPageController.view setFrame:self.view.bounds];
    [self.view addSubview:segmentPageController.view];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [segmentPageController.selectedViewController viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void) toggleGender:(UIButton*)sender
{
    // 스위치 설정은 토글되어 저장되어야 한다.
    if( [[NSUserDefaults standardUserDefaults] boolForKey:@"WinnerSwitch"] )
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"WinnerSwitch"];
        [genderBtn setBackgroundImage:[UIImage imageNamed:@"g_man.png"] forState:UIControlStateNormal];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"WinnerSwitch"];
        [genderBtn setBackgroundImage:[UIImage imageNamed:@"g_woman.png"] forState:UIControlStateNormal];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:genderBtn cache:YES];
    [UIView commitAnimations];

    for( WinnerTableVController *wvc in vArray )
        [wvc removeCurrentListAndCache];

    // 현재 선택된 컨트롤러가 일단 목록을 다시 읽도록 강제한다.
    [segmentPageController.selectedViewController viewWillAppear:YES];

//    [UIView animateWithDuration:0.6
//                     animations:^(void){
//                     }
//                     completion:^(BOOL finish){
//                         if( finish ){
//                         }
//                     }];
    START_WAIT_VIEW;
}

#pragma mark -

- (BOOL)segmentPageController:(JCMSegmentPageController *)segmentController shouldSelectViewController:(UIViewController *)viewController atIndex:(NSUInteger)index
{
	NSLog(@"segmentPageController %@ shouldSelectViewController %@ at index %u", segmentController, viewController, index);
	return YES;
}

- (void)segmentPageController:(JCMSegmentPageController *)segmentController didSelectViewController:(UIViewController *)viewController atIndex:(NSUInteger)index
{
	NSLog(@"segmentPageController %@ didSelectViewController %@ at index %u", segmentController, viewController, index);
}

#pragma mark -

-(void) pushDetailViewCtrl:(UIViewController*) ctrl
{
    [self.navigationController pushViewController:ctrl animated:NO];
}

@end
