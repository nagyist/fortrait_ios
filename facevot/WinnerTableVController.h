//
//  WinnerTableVController.h
//  facevot
//
//  Created by 김태한 on 12. 7. 8..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WinnerCell.h"
#import "WinnerViewController.h"
#import "LKQueue.h"

@interface WinnerTableVController : UITableViewController <UITableViewDataSource, UITableViewDelegate, WinnderCellDelegate>
{
    NSMutableArray  *winList;
    LKQueue         *listQueue;
    NSUInteger      wType;

    UIImageView *tmpImgV, *tmpBackImgV;
    CGRect tRect;
    BOOL endOfList;
    UIButton *footer;
    NSUInteger page;
}

@property (nonatomic, strong) WinnerViewController *superNaviCtrl;

-(id) initWithType:(NSUInteger) typeValue;
- (void) removeCurrentListAndCache;

@end
