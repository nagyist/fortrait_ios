//
//  ImageScrollView.h
//  ImInHotspot
//
//  Created by 태한 김 on 11. 3. 3..
//  Copyright 2011 kth. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ImageScrollView : UIScrollView <UIScrollViewDelegate>
{
    UIImageView  *imageView;
}

@property (assign) NSUInteger index;

-(void) displayImage:(NSString*)imageUrl;
-(void) cancelImageLoading;

@end
