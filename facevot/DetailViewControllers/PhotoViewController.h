//
//  PhotoViewController.h
//  ImInHotspot
//
//  Created by 태한 김 on 11. 3. 3..
//  Copyright 2011 kth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "VoteResultView.h"

@class ImageScrollView;


@interface PhotoViewController : BaseViewController <UIScrollViewDelegate, VoteResultViewDataSource>
{
    UIToolbar *head, *tail;
    UILabel     *nameLabel;
    NSUInteger  currentIdx;

    UIScrollView    *pagingScrollView;

    NSMutableSet    *recycledPages;
    NSMutableSet    *visiblePages;

    NSString *facebookLink;
}

@property (nonatomic, retain) NSMutableArray *listDics;

-(void) setStartIndex:(NSUInteger) index;
-(void) configurePage:(ImageScrollView*)page forIndex:(NSUInteger)index;

//- (void)tilePages;
//
//- (CGRect)frameForPagingScrollView;
//- (CGRect)frameForPageAtIndex:(NSUInteger)index;
//- (CGSize)contentSizeForPagingScrollView;
//
//- (ImageScrollView *)dequeueRecycledPage;
//- (BOOL)isDisplayingPageForIndex:(NSUInteger)index;

@end
