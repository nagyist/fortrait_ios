//
//  MyDetailViewController.m
//  facevot
//
//  Created by 김태한 on 12. 7. 11..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "MyDetailViewController.h"
#import "PCHalfPieChart.h"
#import "YBSwipeViewController.h"
#import "AppDelegate.h"

@implementation MyDetailViewController

- (id)initWithDic:(NSDictionary*) myDic
{
    self = [super init];
    if (self) {
        [self.view setFrame:CGRectMake(0, 0, 320, 200)];
        [self.view setBackgroundColor:[UIColor clearColor]];
        [self.view setUserInteractionEnabled:NO];
        
        dicData = myDic;

        NSUInteger t = 0;
        for( NSNumber *v in dicData[@"votes"] )
            t = t + [v integerValue];
        
        if( 0 == t )
            [self displayNoVoteNow];
        else
            [self setChart];
    }
    return self;
}

- (void) setChart
{
    NSArray *votes = dicData[@"votes"];

    NSArray *vArray = @[
    [PCHalfPieComponent halfPieComponentWithTitle:ITEMSTR_0(dicData[@"sex"]) value:((NSNumber*)votes[0]).integerValue color:COLOR_0],
    [PCHalfPieComponent halfPieComponentWithTitle:ITEMSTR_1(dicData[@"sex"]) value:((NSNumber*)votes[1]).integerValue color:COLOR_1],
    [PCHalfPieComponent halfPieComponentWithTitle:ITEMSTR_2(dicData[@"sex"]) value:((NSNumber*)votes[2]).integerValue color:COLOR_2],
    [PCHalfPieComponent halfPieComponentWithTitle:ITEMSTR_3(dicData[@"sex"]) value:((NSNumber*)votes[3]).integerValue color:COLOR_3],
    [PCHalfPieComponent halfPieComponentWithTitle:ITEMSTR_4(dicData[@"sex"]) value:((NSNumber*)votes[4]).integerValue color:COLOR_4],
    [PCHalfPieComponent halfPieComponentWithTitle:ITEMSTR_5(dicData[@"sex"]) value:((NSNumber*)votes[5]).integerValue color:COLOR_5]
    ];

    PCHalfPieChart *chartView = [[PCHalfPieChart alloc] initWithFrame:CGRectMake(10, 0, 300, 200)];
    [chartView setComponents:[vArray mutableCopy]];

    [self.view addSubview:chartView];
}

-(void) displayNoVoteNow
{
    UILabel *msg = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 200)];
    [msg setTextAlignment:NSTextAlignmentCenter];
    [msg setBackgroundColor:[UIColor clearColor]];
    [msg setTextColor:[UIColor whiteColor]];
    [msg setShadowColor:[UIColor blackColor]];
    [msg setShadowOffset:CGSizeMake(0, 1)];
    [msg setFont:CS_BOLD_FONT(14.0)];
    [msg setText:@"There is no vote until now."];
    [self.view addSubview:msg];
};

@end
