//
//  MyDetailViewController.h
//  facevot
//
//  Created by 김태한 on 12. 7. 11..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+KNSemiModal.h"

@interface MyDetailViewController : UIViewController
{
    NSDictionary *dicData;
}
- (id)initWithDic:(NSDictionary*) myDic;

@end
