//
//  BaseStreamController.h
//  facevot
//
//  Created by 김태한 on 12. 7. 8..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//
//  가장 첫 화면이 되는 사진 목록 화면의 기본 뷰 컨트롤러.

#import <UIKit/UIKit.h>

@interface BaseStreamController : UICollectionViewController
{
    UIButton *addBtn;
    int page;
}

- (void)enableMenuGesture:(BOOL)enable;

@end
