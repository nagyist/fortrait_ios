//
//  DetailViewController.h
//  LeftMenuDemo
//
//  Created by YoonBong Kim on 12. 3. 5..
//  Copyright (c) 2012년 KTH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface DetailViewController : BaseViewController
{
    UIImageView *_imgView;
    UIImage *placeholderImage;
    NSDictionary *info;
    UIViewController *mvc;

    UIToolbar *head, *tail;
    CGFloat _normPointY;
    CGFloat _beginPointY;
    UIActivityIndicatorView *waitIndicator;
    UIImageView *checkMark;

    UILabel *nameLabel;
    NSString *facebookLink;
}

- (id)initWithDic:(NSMutableDictionary*) dicInfo;

- (void) setTempImage:(UIImage*) img;
- (void) setCaller:(id)vc;

@end
