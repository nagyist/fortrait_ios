//
//  BaseViewController.m
//  LeftMenuDemo
//
//  Created by YoonBong Kim on 12. 3. 19..
//  Copyright (c) 2012년 KTH. All rights reserved.
//

#import "BaseViewController.h"
#import "YBSwipeViewController.h"
#import "AppDelegate.h"

@implementation BaseViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        // 타이들은 검은색 글자, 그림자 없음.
        [self.navigationController.navigationBar setTitleTextAttributes:@{UITextAttributeTextColor:[UIColor blackColor], UITextAttributeTextShadowColor:[UIColor clearColor]}];
    }
    return self;
}

- (void)loadView
{
    [super loadView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    /* If instance is first(top) view controller, it can swipe for appearing menu. but if pushed view controller, can't */
    if ([[self.navigationController viewControllers][0] isEqual:self])
    {
        // 테두리 없는 버튼 아이템을 만든다.
        UIButton *mBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 22)];
        [mBtn setBackgroundImage:[UIImage imageNamed:@"menuBtn.png"] forState:UIControlStateNormal];
        [mBtn addTarget:self action:@selector(onMenu) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc] initWithCustomView:mBtn];
        self.navigationItem.leftBarButtonItem = menuBtn;

        [self enableMenuGesture:YES];
    }
    else
    {
        self.navigationItem.leftBarButtonItem = nil;
        
        [self enableMenuGesture:NO];
    }
    // 디자인 시안에 맞춰 이미지 배경을 가진 네비게이션 바를 만든다. 그림자도 없앤다.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationBar.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setClipsToBounds:YES];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (NSUInteger) supportedInterfaceOrientations
{
    return( UIInterfaceOrientationPortrait|
           UIInterfaceOrientationPortraitUpsideDown);
}


/* Menu button selector in left navigation */
- (void)onMenu
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    YBSwipeViewController *swipeViewCntrlr = (YBSwipeViewController *)[[appDelegate window] rootViewController];
    
    switch (swipeViewCntrlr.status) {
        case ViewStatusMain:
            [swipeViewCntrlr setShowMenu:YES animated:YES];
            break;
        case ViewStatusMenu:
            [swipeViewCntrlr setShowMenu:NO animated:YES];
            break;
        default:
            break;
    }
}


/* swipe gesture enable/disable method */
- (void)enableMenuGesture:(BOOL)enable
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    YBSwipeViewController *swipeViewCntrlr = (YBSwipeViewController *)[[appDelegate window] rootViewController];
    swipeViewCntrlr.menuGestureEnable = enable;
}

#pragma mark - Restorable State

-(void) encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];
}

-(void) decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];
}

@end
