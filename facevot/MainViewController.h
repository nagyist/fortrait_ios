//
//  MainViewController.h
//  LeftMenuDemo
//
//  Created by YoonBong Kim on 12. 3. 5..
//  Copyright (c) 2012년 KTH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "LKQueue.h"
#import "MyCell.h"
#import "DetailViewController.h"

@interface MainViewController : BaseViewController <UITableViewDataSource, UITabBarDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, MyCellDelegate>
{
    NSString *theIdentity;
    NSUInteger tmpScore;
    BOOL        theGender;

    UIImagePickerController *imgPicker;
    UILabel *numView;
    UILabel *mostText;
    NSArray *myItemNumsArray;

    NSMutableArray *myList;
    LKQueue *listQueue;

    //
    UIImageView *tmpImgV, *tmpBackImgV;
    CGRect tRect;
    BOOL endOfList;
    UIButton *footer;
    NSUInteger page;
}

- (id) initWithIdentity:(NSString*)identity name:(NSString*)theName;

- (void) insertPhotoUrl:(NSString*)address thumbUrl:(NSString*) thumbAddr;
- (void) removeLatestItem;
- (void) uplodedPhotoID:(NSString*) new_id;
- (void) returnAnimation;

@end
