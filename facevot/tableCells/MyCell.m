//
//  MyCell.m
//  facevot
//
//  Created by 김태한 on 12. 6. 30..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "MyCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation MyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor:[UIColor whiteColor]];
        [self setSelectionStyle:UITableViewCellSelectionStyleGray];

        // Initialization code
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 320-16, MY_CELL_H-8)];
        [imgView setClipsToBounds:YES];
        [imgView setContentMode:UIViewContentModeScaleAspectFill];
        [self.contentView addSubview:imgView];

        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(155, 0, 149, imgView.bounds.size.height)];
        [backView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"myBack.png"]]];
        [imgView addSubview:backView];
        // 가장 표를 많이 얻은 항목의 배경이 되는 사각형 영역.
        focusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, backView.frame.size.width, 25)];
        [focusView setAlpha:0.5];
        [focusView setBackgroundColor:[UIColor blackColor]];
        [backView addSubview:focusView];

        labelArray = @[ [[UILabel alloc]init], [[UILabel alloc]init], [[UILabel alloc] init], [[UILabel alloc] init], [[UILabel alloc] init], [[UILabel alloc]init] ];
        int ix = 0;
        for( UILabel *l in labelArray )
        {
            [l setFont:CS_BOLD_FONT(13)];
            [l setBackgroundColor:[UIColor clearColor]];
            [l setFrame:CGRectMake(191, 13+(ix*25), 70, 15)];
            [l setTextColor:[UIColor whiteColor]];
            [l setTextAlignment:NSTextAlignmentLeft];
            ix++;
            [self.contentView addSubview:l];
        }
        numLabelArray =  @[ [[UILabel alloc]init], [[UILabel alloc]init], [[UILabel alloc] init], [[UILabel alloc] init], [[UILabel alloc] init], [[UILabel alloc]init] ];
        ix = 0;
        for( UILabel *l in numLabelArray )
        {
            [l setFont:CS_BOLD_FONT(13)];
            [l setBackgroundColor:[UIColor clearColor]];
            [l setFrame:CGRectMake(254, 13+(ix*25), 49, 15)];
            [l setTextColor:[UIColor whiteColor]];
            [l setTextAlignment:NSTextAlignmentRight];
            ix ++;
            [self.contentView addSubview:l];
        }
        iconArray = @[ [[UIImageView alloc]init], [[UIImageView alloc]init],[[UIImageView alloc]init],[[UIImageView alloc]init],[[UIImageView alloc]init],[[UIImageView alloc]init] ];
        ix = 0;
        for( UIImageView *v in iconArray )
        {
            [v setFrame:CGRectMake(169, 12+(ix*25), 18, 18)];
            ix++;
            [self.contentView addSubview:v];
        }

        // 이미지 탭을 별도로 확인하기 위한 보이지 않는 버튼.
        imgButton = [[UIButton alloc] initWithFrame:imgView.frame];
        [imgButton addTarget:self action:@selector(imageTap) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:imgButton];
    }
    return self;
}

- (void) setGender:(BOOL)_gender
{
    gender = _gender;

    if( BOY == gender ) {
        [iconArray[0] setImage:[UIImage imageNamed:@"whi_m_sweet.png"]];
        [iconArray[1] setImage:[UIImage imageNamed:@"whi_m_cool.png"]];
        [iconArray[2] setImage:[UIImage imageNamed:@"whi_m_handsome.png"]];
        [iconArray[3] setImage:[UIImage imageNamed:@"whi_m_sexy.png"]];
        [iconArray[4] setImage:[UIImage imageNamed:@"whi_m_tough.png"]];
        [iconArray[5] setImage:[UIImage imageNamed:@"whi_m_strong.png"]];
    } else {
        [iconArray[0] setImage:[UIImage imageNamed:@"whi_g_sexy.png"]];
        [iconArray[1] setImage:[UIImage imageNamed:@"whi_g_cute.png"]];
        [iconArray[2] setImage:[UIImage imageNamed:@"whi_g_gorg.png"]];
        [iconArray[3] setImage:[UIImage imageNamed:@"whi_g_pretty.png"]];
        [iconArray[4] setImage:[UIImage imageNamed:@"whi_g_lovely.png"]];
        [iconArray[5] setImage:[UIImage imageNamed:@"whi_g_beauty.png"]];
    }
}

-(void) setImageURL:(NSString*)url numbers:(NSArray*)nums  index:(NSUInteger)idx
{
    index = idx;
    NSUInteger bigOne = 0;

    // 작은 섬네일용 이미지를 쓰니, 너무 작구나. width 180.
//    if( [url hasSuffix:@"_n.jpg"] )
//        [imgView setImageWithURL:[NSURL URLWithString:[url stringByReplacingCharactersInRange:NSMakeRange(url.length-5, 1) withString:@"a"]]];
//    else
    if( ![url isKindOfClass:[NSNull class]] )
        [imgView setImageWithURL:[NSURL URLWithString:url]];

    for( NSUInteger i = 0; i <= 5; i++ ){
        [labelArray[i] setText:[NSString stringWithFormat:@"%@",ITEM_TXT(gender)[i]]];
        [numLabelArray[i] setText:[NSString stringWithFormat:@"%@", nums[i]]];
        [labelArray[i] setAlpha:0.5];
        [numLabelArray[i] setAlpha:0.5];
        [iconArray[i] setAlpha:0.5];

        NSInteger bn = ((NSNumber*)nums[bigOne]).integerValue;
        NSInteger tn = ((NSNumber*)nums[i]).integerValue;
        if( bn < tn ) {
            bigOne = i;
        }
    }

    // 가장 큰 숫자 항목의 정보를 반투명하지 않게.
    NSLog(@"bigOne : %d %@", bigOne, nums[bigOne]);
    [iconArray[bigOne] setAlpha:1.0];
    [labelArray[bigOne] setAlpha:1.0];
    [numLabelArray[bigOne] setAlpha:1.0];
    [focusView setFrame:CGRectMake(0, bigOne*25, focusView.frame.size.width, 25)];
}

- (void) imageTap
{
    if( self.delegate != nil && [self.delegate respondsToSelector:@selector(selectedMyCell:thumbView:)] )
        [self.delegate selectedMyCell:index thumbView:imgView];
}

@end
