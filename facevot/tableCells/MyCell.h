//
//  MyCell.h
//  facevot
//
//  Created by 김태한 on 12. 6. 30..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCell : UITableViewCell
{
    NSUInteger  index;
    UIImageView *imgView;
    NSArray     *labelArray, *numLabelArray, *iconArray;
    UIButton    *imgButton;
    UIView      *focusView;
    BOOL        gender;
}

- (void) setGender:(BOOL)gender;
- (void) setImageURL:(NSString*)url numbers:(NSArray*) nums index:(NSUInteger)idx;

@property (nonatomic, assign) id delegate;

@end

@protocol MyCellDelegate

- (void) selectedMyCell:(NSUInteger) idx thumbView:(UIImageView*)view;

@end