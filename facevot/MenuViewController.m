//
//  MenuViewController.m
//  LeftMenuDemo
//
//  Created by YoonBong Kim on 12. 3. 5..
//  Copyright (c) 2012년 KTH. All rights reserved.
//

#import "MenuViewController.h"
#import "AppDelegate.h"
#import "YBSwipeViewController.h"
#import "MainViewController.h"
#import "StageViewController.h"
#import "FavoTableViewController.h"
#import "WinnerViewController.h"
#import "SettingViewController.h"

#define kSelectedMenuItem   @"selectedMenuItem"
#define kMenuTableView      @"menuTableView"
#define kMenuNames          @"menuNames"


@implementation MenuViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
//        self.restorationIdentifier = @"rkMenuViewController";
    }
    return self;
}


- (void)loadView
{
    [super loadView];
    
    self.view.backgroundColor = [UIColor greenColor];
    
    [self.navigationController setNavigationBarHidden:YES];

    
    _menuNames = @[ @"Stage", @"My Fortrait", @"Favorites", @"Winners", @"About" ];

    _menuTableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] bounds] style:UITableViewStylePlain];
    [_menuTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_menuTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"menuCell"];
    _menuTableView.dataSource = self;
    _menuTableView.delegate = self;
    [_menuTableView setBackgroundColor:CS_RGB(60, 60, 60)];

    [self.view addSubview:_menuTableView];
    _first = NO;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if( NO == _first ) {
//        [_menuTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
//                                    animated:YES scrollPosition:UITableViewScrollPositionTop];
        _first = YES;
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [coder encodeObject:_menuTableView forKey:kMenuTableView];
    [coder encodeObject:_menuNames forKey:kMenuNames];

    [super encodeRestorableStateWithCoder:coder];
}

-(void) decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];
    _menuNames = [coder decodeObjectForKey:kMenuNames];
    _menuTableView = [coder decodeObjectForKey:kMenuTableView];
    _first = NO;
    _menuTableView.dataSource = self;
    _menuTableView.delegate = self;

    [self.view addSubview:_menuTableView];
}


#pragma mark - UITableView dataSource, delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_menuNames count];
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 38.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell" forIndexPath:indexPath];

    cell.textLabel.text = _menuNames[indexPath.row];
    cell.textLabel.textColor = [UIColor whiteColor];
    [cell.textLabel setFont:CS_BOLD_FONT(15)];
    [cell.textLabel setShadowColor:CS_RGB(45,45,45)];
    [cell.textLabel setShadowOffset:CGSizeMake(0, 1)];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell.contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"menuCellBG.png"]]];

    switch ( indexPath.row ) {
        case 0:
            cell.imageView.image = [UIImage imageNamed:@"i_stage.png"];
            break;
        case 1:
            cell.imageView.image = [UIImage imageNamed:@"i_me.png"];
            break;
        case 2:
            cell.imageView.image = [UIImage imageNamed:@"i_favo.png"];
            break;
        case 3:
            cell.imageView.image = [UIImage imageNamed:@"i_win.png"];
            break;
        case 4:
            cell.imageView.image = [UIImage imageNamed:@"i_about.png"];
            break;
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    YBSwipeViewController *swipeViewCntrlr = (YBSwipeViewController *)[appDelegate.window rootViewController];
    
    UIViewController *viewCntrlr = nil;

    switch ( indexPath.row ) {
        case 0:
            viewCntrlr = [[StageViewController alloc] init];
            break;
        case 1:
            viewCntrlr = [[MainViewController alloc] initWithIdentity:[[NSUserDefaults standardUserDefaults] objectForKey:@"myIdentity"] name:@"My Fortrait"];
            break;
        case 2:
            viewCntrlr = [[FavoTableViewController alloc] init];
            break;
        case 3:
            viewCntrlr = [[WinnerViewController alloc] init];
            break;
        case 4:
            viewCntrlr = [[SettingViewController alloc] init];
            break;

        default:
            return;
    }
    if( 0 != indexPath.row )
        [viewCntrlr setTitle:_menuNames[indexPath.row]];

    [swipeViewCntrlr setSelectMenu:viewCntrlr animated:YES];
        
}

@end
