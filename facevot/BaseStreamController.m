//
//  BaseStreamController.m
//  facevot
//
//  Created by 김태한 on 12. 7. 8..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "BaseStreamController.h"
#import "YBSwipeViewController.h"
#import "AppDelegate.h"

@implementation BaseStreamController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    /* If instance is first(top) view controller, it can swipe for appearing menu. but if pushed view controller, can't */
    if ([[self.navigationController viewControllers][0] isEqual:self])
    {
        // 테두리 없는 버튼 아이템을 만든다.
        UIButton *mBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 22)];
        [mBtn setBackgroundImage:[UIImage imageNamed:@"menuBtn.png"] forState:UIControlStateNormal];
        [mBtn addTarget:self action:@selector(onMenu) forControlEvents:UIControlEventTouchUpInside];

        UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc] initWithCustomView:mBtn];
        self.navigationItem.leftBarButtonItem = menuBtn;
        
        [self enableMenuGesture:YES];
    }
    else
    {
        self.navigationItem.leftBarButtonItem = nil;
        
        [self enableMenuGesture:NO];
    }

    // 디자인 시안에 맞춰 이미지 배경을 가진 네비게이션 바를 만든다. 그림자도 없앤다.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationBar.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setClipsToBounds:YES];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];

    // 타이들은 검은색 글자, 그림자 없음.
    [self.navigationController.navigationBar setTitleTextAttributes:@{UITextAttributeTextColor:[UIColor blackColor], UITextAttributeTextShadowColor:[UIColor clearColor]}];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [addBtn removeFromSuperview];
    addBtn = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* Menu button selector in left navigation */
- (void)onMenu
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    YBSwipeViewController *swipeViewCntrlr = (YBSwipeViewController *)[[appDelegate window] rootViewController];
    
    switch (swipeViewCntrlr.status) {
        case ViewStatusMain:
            [swipeViewCntrlr setShowMenu:YES animated:YES];
            break;
        case ViewStatusMenu:
            [swipeViewCntrlr setShowMenu:NO animated:YES];
            break;
        default:
            break;
    }
}


/* swipe gesture enable/disable method */
- (void)enableMenuGesture:(BOOL)enable
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    YBSwipeViewController *swipeViewCntrlr = (YBSwipeViewController *)[[appDelegate window] rootViewController];
    swipeViewCntrlr.menuGestureEnable = enable;
}

@end
