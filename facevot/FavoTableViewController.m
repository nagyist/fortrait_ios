//
//  FavoTableViewController.m
//  facevot
//
//  Created by 김태한 on 12. 6. 30..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "FavoTableViewController.h"
#import "FavorCell.h"
#import "NetInterface.h"
#import "LKQueueManager.h"
#import "MainViewController.h"

#define MY_FQUEUE_ID    @"favoriteQueue"

@implementation FavoTableViewController

-(id) init
{
    self = [super init];
    if( self ) {
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self.tableView registerClass:[FavorCell class] forCellReuseIdentifier:@"favorateCell"];

        UIRefreshControl *refreshCtrl = [[UIRefreshControl alloc] init];
        [refreshCtrl setTintColor:[UIColor darkGrayColor]];
        [refreshCtrl addTarget:self action:@selector(refreshMe:) forControlEvents:UIControlEventValueChanged];
        [self setRefreshControl:refreshCtrl];

        fList = [[NSMutableArray alloc] initWithCapacity:20];
        fQueue = [[LKQueueManager defaultManager] queueWithName:MY_FQUEUE_ID];
    }
    return self;
}

-(void) loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor grayColor];

    UIBarButtonItem *editBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(toggleEditAction)];
    self.navigationItem.rightBarButtonItem = editBtn;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if( 0 == [fList count] ) {
        page = 1;
        
        LKQueueEntry *entry = [fQueue entryAtIndex:0];
        [fList addObjectsFromArray:(NSArray*)(entry.resources)];
//        if( 20 > [fList count] )
//            endOfList = YES;
    }
    // 다른 곳에서 즐겨찾기 항목에 변화가 생겼다면 네트워크에서 다시 읽자.
    if( USERCONTEXT.needReloadFavo )
        [self refreshMe:self.refreshControl];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void) toggleEditAction
{
    if( self.tableView.editing )
    {
        [self.tableView setEditing:NO];
        UIBarButtonItem *editBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(toggleEditAction)];
        self.navigationItem.rightBarButtonItem = editBtn;
    }
    else
    {
        [self.tableView setEditing:YES];
        UIBarButtonItem *editBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(toggleEditAction)];
        self.navigationItem.rightBarButtonItem = editBtn;
    }
}

#pragma mark - UITableView dataSource, delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [fList count];
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FAVO_CELL_H;
}

- (UITableViewCell *)tableView:(UITableView *)tView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FavorCell *cell = [tView dequeueReusableCellWithIdentifier:@"favorateCell" forIndexPath:indexPath];

    NSDictionary *data = fList[indexPath.row];

    [cell setImageURL:data[@"url"] name:data[@"name"]];

    return cell;
}

// 선택된 사람의 상세 페이지로 들어감.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSDictionary *data = [fList objectAtIndex:indexPath.row];

    MainViewController *mvc = [[MainViewController alloc] initWithIdentity:data[@"identity"] name:data[@"name"]];
    [self.navigationController pushViewController:mvc animated:YES];
}

#pragma mark -

- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // TODO:
}

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ( UITableViewCellEditingStyleDelete );
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( UITableViewCellEditingStyleDelete != editingStyle ) return;

    NSDictionary *data = [fList objectAtIndex:indexPath.row];

    //서버로 항목 삭제 DELETE
    [NetInterface setFavoriteManId:data[@"identity"]
                      withPhottoId:data[@"id"]
                         boolValue:NO
                              name:data[@"name"]
                        onComplete:^(BOOL result)
     {
         [fList removeObjectAtIndex:indexPath.row];
         
         // 지운 목록을 캐쉬에 다시 저장.
         [fQueue removeAllEntries];
         [fQueue addEntryWithInfo:@{@"title":@"MyFavoList"}
                        resources:fList
                          tagName:nil];
     }
                           onError:^()
     {
         NOTI(@"Network Error");
     }];
}

#pragma mark - refresh

- (void) refreshMe:(id) refreshCtrl
{
    NSLog(@"refresh notification.");
    page = 1;

    [NetInterface favoTimelinePage:1
                        onComplete:^(NSArray *myFavoArray)
     {
         [fList removeAllObjects];
         [fList addObjectsFromArray:myFavoArray];
         
//         if( 20 > [myDic[@"photos"] count] )
//             endOfList = YES;

         [fQueue removeAllEntries];
         [fQueue addEntryWithInfo:@{@"title":@"MyFavoList"}
                           resources:fList
                             tagName:nil];

         USERCONTEXT.needReloadFavo = NO;
         
         dispatch_async(dispatch_get_main_queue(), ^(){
             [refreshCtrl endRefreshing];
             [self.tableView reloadData];
         });
     }
                           onError:^(void)
     {
         NOTI(@"Network Error");
         [refreshCtrl endRefreshing];
     }];
}

// 현재 즐겨찾기 목록은 페이지 단위로 읽지 않아서 쓰이지 않는 메소드 임.
- (void) readMore
{
    NSLog(@"read more.");
    page++;

    [NetInterface favoTimelinePage:page
                      onComplete:^(NSArray *myFavoArray)
     {
         [fList addObjectsFromArray:myFavoArray];
         
//         if( 20 > [myDic[@"photos"] count] )
//             endOfList = YES;

         dispatch_async(dispatch_get_main_queue(), ^(){
             [self.tableView reloadData];
         });
     }
                         onError:^(void)
     {
         NOTI(@"Network Error");
     }];
}
@end
