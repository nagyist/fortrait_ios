//
//  NetInterface.m
//  facevot
//
//  Created by 김태한 on 12. 7. 14..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "NetInterface.h"
#import "AppDelegate.h"

#define BASEURL_  @"http://facevotapi.appspot.com"


@implementation NetInterface


+ (void) publicTimelineGender:(BOOL)gender page:(NSUInteger) page
                   onComplete:(completeHandler) handler
                      onError:( void(^)(NSError*)) errHandler
{
    NSMutableString *address = [[NSMutableString alloc] initWithString:BASEURL_];
    [address appendString:@"/photo"];
    [address appendFormat:@"?page=%d&sex=%@",page,@(gender)];
    if( [[NSUserDefaults standardUserDefaults] objectForKey:@"myIdentity"] )
        [address appendFormat:@"&identity=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"myIdentity"]];

    NSURL *url = [NSURL URLWithString:address];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    NSLog(@"pb> %@",address);

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"Public Timeline: %@", JSON);
        handler( [JSON objectForKey:@"photos"]);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        // 404 에러는 그냥 빈 목록으로 처리되도록.
//        if( 404 == error.code )
//            handler( @[] );
//        else
        if(errHandler)
            errHandler( error );
    }];

    [operation start];
}

+ (void) myTimelinePage:(NSUInteger) page
               identity:(NSString*) identity
             OnComplete:(void(^)(NSDictionary*)) handler
                onError:( void(^)(NSError*)) errHandler
{
    NSMutableString *address = [[NSMutableString alloc] initWithString:BASEURL_];
    [address appendString:@"/photo/mine"];
    [address appendFormat:@"?page=%d",page];
    [address appendFormat:@"&identity=%@",identity];

    NSURL *url = [NSURL URLWithString:address];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    NSLog(@"> %@",address);

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"my Timeline: %@", JSON);
        handler( JSON );
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(errHandler)
            errHandler( error );
    }];
    
    [operation start];
}

+ (void) weeklyWinnerGender:(BOOL)gender page:(NSUInteger) page
                 onComplete:( void(^)(NSDictionary*)) handler
                    onError:( void(^)(void)) errHandler
{
    NSMutableString *address = [[NSMutableString alloc] initWithString:BASEURL_];
    [address appendFormat:@"/vote/weekly?page=%d",page];
    [address appendFormat:@"&identity=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"myIdentity"]];

    NSURL *url = [NSURL URLWithString:address];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"weekly : %@", JSON);
        handler( JSON );
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(errHandler)
            errHandler();
    }];

    [operation start];
}

+ (void) monthlyWinnerGender:(BOOL)gender page:(NSUInteger) page
                  onComplete:( void(^)(NSDictionary*)) handler
                     onError:( void(^)(void)) errHandler
{
    NSMutableString *address = [[NSMutableString alloc] initWithString:BASEURL_];
    [address appendFormat:@"/vote/monthly?page=%d",page];
    [address appendFormat:@"&identity=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"myIdentity"]];

    NSURL *url = [NSURL URLWithString:address];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"monthly : %@", JSON);
        handler( JSON );
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(errHandler)
            errHandler();
    }];

    [operation start];
}

+ (void) totalWinnerGender:(BOOL)gender page:(NSUInteger) page
                onComplete:( void(^)(NSDictionary*)) handler
                   onError:( void(^)(void)) errHandler
{
    NSMutableString *address = [[NSMutableString alloc] initWithString:BASEURL_];
    [address appendFormat:@"/vote/total?page=%d",page];
    [address appendFormat:@"&identity=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"myIdentity"]];

    NSURL *url = [NSURL URLWithString:address];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"total : %@", JSON);
        handler( JSON );
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(errHandler)
            errHandler();
    }];
    
    [operation start];
}

+ (void) favoTimelinePage:(NSUInteger) page
               onComplete:(completeHandler) handler
                  onError:( void(^)(void)) errHandler
{
    NSMutableString *address = [[NSMutableString alloc] initWithString:BASEURL_];
    [address appendFormat:@"/photo/favorite?identity=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"myIdentity"]];


    NSURL *url = [NSURL URLWithString:address];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"favorite : %@", JSON);
        handler( [JSON objectForKey:@"favorites"] );
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(errHandler)
            errHandler();
    }];

    [operation start];
}

/*PUT
 /photo/favorite?photoid=1&favoriteid=wonkim&identity=tester&name=wonil
 DELETE
 /photo/favorite?identity=tester&favoriteid=wonkim
 */
+ (void) setFavoriteManId:(NSString*) targetId
             withPhottoId:(NSString*) photoId
                boolValue:(BOOL)setVal
                     name:(NSString*) herName
               onComplete:( void(^)(BOOL)) handler
                  onError:( void(^)(void)) errHandler
{
    NSMutableString *address = [[NSMutableString alloc] initWithString:BASEURL_];
    [address appendFormat:@"/photo/favorite?identity=%@",USERCONTEXT.myIdentity];
    [address appendFormat:@"&favoriteid=%@", targetId];
    [address appendFormat:@"&photoid=%@", photoId];
    [address appendFormat:@"&name=%@", herName];

    NSLog(@"favo: %@", address);

    NSURL *url = [NSURL URLWithString:address];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];

    if( setVal )    // add favorite
    {
        [request setHTTPMethod:@"PUT"];
    }
    else            // delete favorite
    {
        [request setHTTPMethod:@"DELETE"];
    }

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        handler( setVal );
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(errHandler)
            errHandler();
    }];

    [operation start];
}

+ (void) postMyPhotoURL:(NSString*)urlString
               thumbURL:(NSString*)thumbUrlString
             onComplete:( void(^)(NSString*) ) handler
                onError:( void(^)(NSError*)) errHandler
{
    NSMutableString *address = [[NSMutableString alloc] initWithString:BASEURL_];
    [address appendFormat:@"/photo?identity=%@",USERCONTEXT.myIdentity];
    [address appendFormat:@"&url=%@",[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [address appendFormat:@"&url2=%@",[thumbUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [address appendFormat:@"&sex=%d",(int)USERCONTEXT.myGender];
    [address appendString:@"&skin=0"];

    NSLog(@">thumb:%@", thumbUrlString);

    NSURL *url = [NSURL URLWithString:address];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"PUT"];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"post : %@", JSON);
        handler([JSON objectForKey:@"result"]);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        if(errHandler)
            errHandler( error );
    }];
    
    [operation start];
}

+ (void) didVoteAtPhotoID:(NSString*)photoId
               onComplete:( void(^)(BOOL)) handler
                  onError:( void(^)(void)) errHandler
{
    NSMutableString *address = [[NSMutableString alloc] initWithString:BASEURL_];
    [address appendFormat:@"/vote?photoid=%@",photoId];
    [address appendFormat:@"&identity=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"myIdentity"]];
    
    NSURL *url = [NSURL URLWithString:address];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSLog(@"> %@",address);
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"did I vote ? : %@", JSON);
        NSString *result = [(NSDictionary*)JSON objectForKey:@"result"];
        handler( [(NSString*)result isEqualToString:@"yes"] );
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        // 404 에러는 그냥 no 값으로 처리되도록.
//        if( 404 == error.code )
//            handler( NO );
//        else
        if(errHandler)
            errHandler();
    }];

    [operation start];
}


+ (void) doVoteAtPhotoID:(NSString*)photoId vote:(NSUInteger)vote
              onComplete:( void(^)(void)) handler
                 onError:( void(^)(void)) errHandler
{
    NSMutableString *address = [[NSMutableString alloc] initWithString:BASEURL_];
    [address appendFormat:@"/vote?photoid=%@",photoId];
    [address appendFormat:@"&votetype=%d", vote];
    [address appendFormat:@"&identity=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"myIdentity"]];

    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:USERCONTEXT.myLocale];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSString *country = [usLocale displayNameForKey:NSLocaleCountryCode value:[locale objectForKey:NSLocaleCountryCode]];
    [address appendFormat:@"&country=%@", [country stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    NSURL *url = [NSURL URLWithString:address];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"PUT"];

    NSLog(@"vote > %@",address);
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"vote ? : %@", JSON);
        handler();
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        NSLog(@"vote err:%@ code:%d", response, error.code );
        if(errHandler)
            errHandler();
    }];
    
    [operation start];
}

+ (BOOL) facebookLogin:( void(^)(BOOL)) handler showUI:(BOOL)showIt
{
    START_WAIT_VIEW;

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return [appDelegate openSessionWithAllowLoginUI:YES handler:handler];
}

@end
