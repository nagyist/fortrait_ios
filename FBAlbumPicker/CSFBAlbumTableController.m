//
//  CSFBAlbumTableController.m
//  facevot
//
//  Created by 김태한 on 12. 8. 14..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "CSFBAlbumTableController.h"
#import "CSFBAlbumCell.h"
#import "CSFBThumbViewController.h"
#import "YBSwipeViewController.h"
#import "AppDelegate.h"

@implementation CSFBAlbumTableController

- (id)init
{
    self = [super init];
    if (self) {
        self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];

        // Initialization code here.
        [self setTitle:@"Facebook Albums"];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = THEME_COLOR;
        self.tableView.separatorColor = THEME_COLOR;

        [self.tableView registerClass:[CSFBAlbumCell class] forCellReuseIdentifier:@"FBalbumCell"];
    }

    START_WAIT_VIEW;
    FBRequest *req = [FBRequest requestForGraphPath:@"me/albums"];
    [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
//    [FBRequest startWithGraphPath:@"me/albums" completionHandler:^(FBRequestConnection *connection, NSDictionary *result, NSError *error)
//     {
         albumList = [[NSMutableArray alloc] initWithCapacity:10];

         // count 값이 0이 아닌 것만 목록으로 구성한다.
         for( NSDictionary *d in [result objectForKey:@"data"] )
         {
             if( 0 != d[@"count"] )
                 [albumList addObject:d];
         }
         dispatch_async(dispatch_get_main_queue(), ^(){
             STOP_WAIT_VIEW;
             [self.tableView reloadData];
         });
     }];

    return self;
}

- (void) loadView
{
    UIBarButtonItem *picButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(closeAction:)];
    self.navigationItem.rightBarButtonItem = picButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTintColor:THEME_COLOR];
}

#pragma mark - UITableView dataSource, delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [albumList count];
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FBALBUM_CELL_H;
}

/*
 {
 "can_upload" = 0;
 count = 1;
 "cover_photo" = 103902149756426;
 "created_time" = "2012-07-15T14:21:20+0000";
 from =             {
     id = 100004099236924;
     name = "Kim Sergei";
 };
 id = 103902143089760;
 link = "http://www.facebook.com/album.php?fbid=103902143089760&id=100004099236924&aid=7594";
 name = "Profile Pictures";
 privacy = everyone;
 type = profile;
 "updated_time" = "2012-07-15T14:21:23+0000";
 }
 */
- (UITableViewCell *)tableView:(UITableView *)tView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSFBAlbumCell *fCell = [tView dequeueReusableCellWithIdentifier:@"FBalbumCell"];
    NSDictionary *cDic = albumList[indexPath.row];

    NSString *imageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=thumbnail&access_token=%@", [cDic objectForKey:@"id"], FBSession.activeSession.accessToken];

    [fCell setImageURL:imageURL name:cDic[@"name"]];

    return fCell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cDic = albumList[indexPath.row];

    CSFBThumbViewController *tVC = [[CSFBThumbViewController alloc] initWithStyle:UITableViewStylePlain title:cDic[@"name"] albumId:cDic[@"id"]];
    [self.navigationController pushViewController:tVC animated:YES];
}

- (void) closeAction: (id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    YBSwipeViewController *swipeViewCntrlr = (YBSwipeViewController *)[[appDelegate window] rootViewController];
    swipeViewCntrlr.menuGestureEnable = YES;
}

@end
