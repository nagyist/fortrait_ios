//
//  CSFBAlbumCell.m
//  facevot
//
//  Created by 김태한 on 12. 6. 30..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import "CSFBAlbumCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation CSFBAlbumCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor:CS_RGB(213, 213, 213)];
        [self setSelectionStyle:UITableViewCellSelectionStyleGray];

        // Initialization code
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake(1, 1, FBALBUM_CELL_H, FBALBUM_CELL_H-2)];
        imgView.layer.borderColor = [UIColor blackColor].CGColor;
        imgView.layer.borderWidth = 1.0f;
        [imgView setClipsToBounds:YES];
        [imgView setContentMode:UIViewContentModeScaleAspectFill];
        [self.contentView addSubview:imgView];

        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(imgView.frame.size.width+5, 0, 310-imgView.frame.size.width, FBALBUM_CELL_H)];
        [nameLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [nameLabel setNumberOfLines:4];
        [nameLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:nameLabel];

        UIImageView *acView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 30, FBALBUM_CELL_H/2-12, 24, 24)];
        [acView setImage:[UIImage imageNamed:@"cell_arrow.png"]];
        [self.contentView addSubview:acView];
    }
    return self;
}

-(void) setImageURL:(NSString*)url name:(NSString*)name
{
    if( ![url isKindOfClass:[NSNull class]] )
        [imgView setImageWithURL:[NSURL URLWithString:url]];

    [nameLabel setText:name];
}

@end
