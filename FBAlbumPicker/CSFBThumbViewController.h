//
//  CSFBThumbViewController.h
//  facevot
//
//  Created by 태한 김 on 12. 8. 14..
//  Copyright (c) 2012년 ChocolateSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSFBThumbViewController : UITableViewController
{
    NSArray *picArray;
}

- (id)initWithStyle:(UITableViewStyle)style title:(NSString*)title albumId:(NSString*)theId;

@end
